import React from 'react';
import { AppRegistry } from 'react-native';
import SideMenu from './src/components/SideMenu';

const App = () => <SideMenu />;

AppRegistry.registerComponent('albums', () => App);
