import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { Right, Left, Icon } from 'native-base';
import EpisodeList from '../components/EpisodeList';
import Header from '../components/header';

class HomeScreen extends Component {
  static navigationOptions = {
    drawerIcon: ({ tintColor }) => (
      <Icon name='home' style={{ fontSize: 24, color: tintColor }} />
    )
  };
  render() {
    return (
      <View style={styles.container}>
        <Header {...this.props} />
        <View>
          <Image
            source={{
              uri:
                'https://images.pexels.com/photos/754082/pexels-photo-754082.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'
            }}
            style={{ width: 400, height: 700, position: 'absolute' }}
          />

          <EpisodeList />
        </View>
      </View>
    );
  }
}

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  menu: {
    color: 'white',
    zIndex: 999,
    width: 100,
    height: 100
  }
});
