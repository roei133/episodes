import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Icon } from 'native-base';

class FavoritesScreen extends Component {
    static navigationOptions = {
        drawerIcon: ({ tintColor }) => (
          <Icon name='ios-eye' style={{ fontSize: 24, color: tintColor }} />
        )
      }

  render() {
    return (
      <View style={styles.container}>
        <Text>FavoritesScreen</Text>
      </View>
    );
  }
}

export default FavoritesScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  }
});
