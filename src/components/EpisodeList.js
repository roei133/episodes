import React, { Component } from 'react';
import {
  ScrollView,
  FlatList,
  Text,
  View,
  Image,
  RefreshControl
} from 'react-native';
import axios from 'axios';
import EpisodeDetail from './EpisodeDetail';
import { Spinner } from './Spinner';

class EpisodeList extends Component {
  state = { episodes: [] };

  componentWillMount() {
    axios
      .get('http://api.tvmaze.com/shows/1/episodes')
      .then(Response => this.setState({ episodes: Response.data }));
  }

  renderEpisodes({ item, index }) {
    return <EpisodeDetail key={index} episode={item} />;
  }

  render() {
    return (
      <FlatList
        data={this.state.episodes}
        renderItem={this.renderEpisodes}
        keyExtractor={episode => episode.name}
        scrollEnabled={this.state.scrollEnabled}
      />
    );
  }
}

export default EpisodeList;
