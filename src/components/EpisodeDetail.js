import React, { Component } from 'react';
import { Text, View, Image, Linking } from 'react-native';
import Card from './Card';
import CardSection from './CardSection';
import Button from './Button';

const EpisodeDetail = ({ episode }) => {
  const { name, image, airdate, medium, summary, url } = episode;
  const {
    headerContentStyle,
    mediumStyle,
    imageContainerStyle,
    headerTextStyle,
    originalStyle
  } = styles;

  return (
    <Card>
      <CardSection>
        <View style={imageContainerStyle}>
          <Image style={mediumStyle} source={{ uri: medium }} />
        </View>

        <View style={headerContentStyle}>
          <Text style={headerTextStyle}>{name}</Text>
          <Text style={{ color: 'white' }}>{airdate}</Text>
        </View>
      </CardSection>

      <CardSection>
        <Image style={originalStyle} source={{ uri: image.original }} />
        <Text style={{ color: 'white' }}>{summary}</Text>
      </CardSection>

      <CardSection>
        <Button onPress={() => Linking.openURL(url)} />
      </CardSection>
    </Card>
  );
};

const styles = {
  headerContentStyle: {
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  headerTextStyle: {
    fontSize: 20,
    color: 'white'
  },

  mediumStyle: {
    height: 50,
    width: 50
  },
  imageContainerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10,
    marginRight: 10
  },
  originalStyle: {
    height: 300,
    flex: 1,
    width: null
  }
};

export default EpisodeDetail;
