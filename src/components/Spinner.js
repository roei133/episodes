import React from 'react';
import { View, ActivityIndicator, Text } from 'react-native';

const Spinner = ({ size }) => (
  <View style={Styles.spinnerStyle}>
    <ActivityIndicator
      flex={1}
      padding={10}
      color='yellow'
      size={size || 'large'}
    />
    <View>
      <Text style={Styles.textOfSpinner}>Data is loading</Text>
    </View>
  </View>
);

const Styles = {
  spinnerStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 70
  },
  textOfSpinner: {
    fontSize: 30,
    color: 'white',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
};

export { Spinner };
