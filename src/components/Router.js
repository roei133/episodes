import React from 'react';
import { Scene, Router } from 'react-native-router-flux';
import EpisodeDetail from './EpisodeDetail';

const RouterComponent = () => (
        <Router>
            <Scene key="root">
            <Scene key="firstpage" component={EpisodeDetail} title="Choose episode" />
            </Scene>
        </Router>

    );

export default RouterComponent;
