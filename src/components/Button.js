import React from 'react';
import { Text, TouchableOpacity } from 'react-native';


const Button = ({ onPress }) => (
    <TouchableOpacity onPress={onPress} style={styles.buttonStyle}>
        <Text style={styles.textStyle}>More Info</Text>
    </TouchableOpacity>
    );


    const styles = {
        buttonStyle: {
            flex: 1,
            alignSelf: 'stretch',
            backgroundColor: '#fff',
            borderRadius: 50,
            borderWidth: 1,
            borderColor: '#007aff',
            marginLeft: 5,
            marginRight: 5,
            paddingTop: 10,
            paddingBottom: 10
        },

        textStyle: {
            alignSelf: 'center',
            color: '#007aff',
            fontSize: 18,
            fontWeight: '600'
        }

    };

export default Button;
