import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';
import { Avatar } from 'react-native-elements';
import { Left, Icon } from 'native-base';
import { DrawerActions } from 'react-navigation';


  class Header extends Component {
    render() {
      return (
        <View style={styles.viewStyle}>
          <Left>
            <Icon
              name='menu'
              style={styles.menu}
              onPress={() => this.props.navigation.dispatch(DrawerActions.openDrawer())}
            />
          </Left>

          <Text style={styles.textStyle}>Episodes</Text>
        </View>
      );
    }
  }
  

export default Header;

const styles = {
  textStyle: {
    fontSize: 30,
    color: 'black',
    fontFamily: 'cursive'
  },

  viewStyle: {
    backgroundColor: '#00BFFF',
    justifyContent: 'flex-start',
    alignItems: 'center',
    height: 60,
    paddingTop: 20,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    elevation: 20,
    positoin: 'relative'
  },
  menu: {
    marginLeft: -160,
    color: 'white'
  }
};


//** */


  /* <Avatar
        size='large'
        overlayContainerStyle={{ backgroundColor: '#00BFFF' }}
        containerStyle={{ marginLeft: 250, marginTop: -40 }}
        onPress={() => console.log('Works!')}
        activeOpacity={0.2}
        rounded
        source={{
            uri:
              'https://banner2.kisspng.com/20180210/ioq/kisspng-popcorn-cartoon-film-drawing-vector-popcorn-5a7ef0733b81e2.7519829615182685312438.jpg'
          }}
        /> */
