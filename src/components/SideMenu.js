import {
  createDrawerNavigator,
  DrawerItems,
  createAppContainer
} from 'react-navigation';
import { Avatar } from 'react-native-elements';
import React, { Component } from 'react';
import { View, Text, SafeAreaView, ScrollView } from 'react-native';
import FavoritesScreen from '../screens/FavoritesScreen';
import HomeScreen from '../screens/HomeScreen';

class SideMenu extends Component {
  render() {
    return <AppDrawerNavigator />;
  }
}

const CustomDraweNavigator = props => (
  <SafeAreaView style={{ flex: 1 }}>
    <View
      style={{
        height: 150,
        backgroundColor: '#00BFFF',
        alignItems: 'center',
        justifyContent: 'center'
      }}
    >
      <Avatar
        size='large'
        overlayContainerStyle={{ backgroundColor: '#00BFFF' }}
        containerStyle={{ marginTop: 30 }}
        activeOpacity={0.2}
        rounded
        source={{
          uri:
            'https://banner2.kisspng.com/20180210/ioq/kisspng-popcorn-cartoon-film-drawing-vector-popcorn-5a7ef0733b81e2.7519829615182685312438.jpg'
        }}
      />
      <View>
        <Text style={{ fontSize: 30 }}>Side Menu</Text>
      </View>
    </View>
    <ScrollView>
      <DrawerItems {...props} />
    </ScrollView>
  </SafeAreaView>
);

const AppDrawerNavigator = createAppContainer(
  createDrawerNavigator(
    {
      Home: HomeScreen,
      Favorites: FavoritesScreen
    },
    {
      contentComponent: CustomDraweNavigator,
      contentOptions: {
        activeTintColor: 'red'
      }
    }
  )
);

export default SideMenu;
