import React from 'react';
import { View } from 'react-native';

const CardSection = props => (
  <View style={styles.containerStyle}>{props.children}</View>
);

const styles = {
  containerStyle: {
    borderBottomWidth: 1,
    padding: 5,
    justifyContent: 'flex-start',
    sflexDirection: 'row',
    borderColor: '#ddd',
    position: 'relative',
    backgroundColor: '#2F4F4F'
  }
};

export default CardSection;
